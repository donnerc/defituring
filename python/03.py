'''

Défi Turing 03
==============

Les facteurs premiers du nombre 2013 sont 3, 11 et 61, car 3 x 11 x 61 = 2013.

Quel est le plus grand facteur premier du nombre 20130101 ?

'''

import time


def turing_3():

    N = 20130101

    reponse = 0
    facteurs = []

    for k in range(3, int(N ** .5) + 2, 2):
        while N % k == 0:
            facteurs += [k]
            N = N // k

    if N != 1:
        facteurs += [N]

    reponse = facteurs[-1]

    print("facteurs de ", N, " : ", facteurs)

    return reponse


if __name__ == '__main__':
    t1 = time.clock()
    reponse = turing_3()
    t2 = time.clock()
    print(reponse)
    print("Temps nécessaire pour trouver la réponse : ", (t2-t1), "secondes")
