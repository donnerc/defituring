'''

Défi Turing 02
==============

Somme des multiples de 5 ou de 7

Si on liste tous les entiers naturels inférieurs à 20 qui sont multiples de 5
ou de 7, on obtient 5, 7, 10, 14 et 15. La somme de ces nombres est 51.

Trouver la somme de tous les multiples de 5 ou de 7 inférieurs à 2013.

'''

def turing_2():

    reponse = 0
    ok = False
    
    i = 0
    j = 0 
    k = 1

    while k <= 4e6:
        if k % 2 == 1:
            reponse += k

        i = j
        j = k
        k = i + j

    return reponse


if __name__ == '__main__':
    reponse = turing_2()
    print(reponse)