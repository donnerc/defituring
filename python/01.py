'''

Défi Turing 01
==============

Somme des multiples de 5 ou de 7

Si on liste tous les entiers naturels inférieurs à 20 qui sont multiples de 5
ou de 7, on obtient 5, 7, 10, 14 et 15. La somme de ces nombres est 51.

Trouver la somme de tous les multiples de 5 ou de 7 inférieurs à 2013.

'''

def turing_1():

    reponse = 0

    for n in range(1, 2013):
        if n % 5 == 0 or n % 7 == 0:
            reponse += n 


    return reponse