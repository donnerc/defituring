'''

Défi Turing 05
==============

Somme des chiffres de 2^2222

2^15 = 32768 et la somme de ses chiffres vaut 3 + 2 + 7 + 6 + 8 = 26.

Que vaut la somme des chiffres composant le nombre 2^2222?

'''


def turing_5():

    reponse = sum([int(n) for n in str(2 ** 2222)])

    return reponse


if __name__ == '__main__':
    reponse = turing_5()
    print(reponse)
