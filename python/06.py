'''

Défi Turing 06
==============

Somme des chiffres du nombre 2013!
n! signifie n x (n-1) x ... x 3 x 2 x 1
Par exemple, 10! = 10 x 9 x ... x 3 x 2 x 1 = 3628800.
La somme des chiffres du nombre 10! vaut 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Trouver la somme des chiffres du nombre 2013!

'''


def factorial(n):

    result = 1
    while n > 0:
        result *= n
        n -= 1

    return result


def turing_6():

    reponse = sum([int(n) for n in str(factorial(2013))])

    return reponse


if __name__ == '__main__':
    reponse = turing_6()
    print(reponse)