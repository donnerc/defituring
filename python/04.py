'''

Défi Turing 04
==============

Le plus grand palindrome en multipliant deux nombres

Un nombre palindrome se lit de la même façon de gauche à droite et de droite à
gauche.

Le plus grand palindrome que l'on peut obtenir en multipliant deux nombres de
deux chiffres est 9009 = 99 x 91.

Quel est le plus grand palindrome que l'on peut obtenir en multipliant un
nombre de 4 chiffres avec un nombre de 3 chiffres ?

'''


def is_palindrome(string):
    return string == ''.join([c for c in reversed(string)])

def test_is_palindrome():
    tests = (
        'abba',
        )

    for t in tests:
        print(t, is_palindrome(t))


def turing_4():

    # en toutes vraisemblance, le palindrome cherché sera multiples de grands
    # nombres. Il faut donc commencer par tester les plus grandes
    # combinaisons. Il est donc inutile de tester tous les éléments du produit
    # cartésien de A x B, car beaucoup de ces éléments sont bien trop petits.
    # La variable STEP permet d'énumérer d'abord toutes les plus grandes
    # combinaisons.
    STEP = 300

    valid = True
    reponse = 0
    a = 9999
    b = 999

    while valid:

        for i in range(STEP):
            for j in range(STEP):
                guess = (a-i) * (b-j)
                if is_palindrome(str(guess)) and guess > reponse:
                    reponse = guess

        if len(str(a)) < 4 or len(str(b)) < 3:
            valid = False

        a -= STEP
        b -= STEP

    return reponse


if __name__ == '__main__':
    reponse = turing_4()
    print(reponse)

    test_is_palindrome()