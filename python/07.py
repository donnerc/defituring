'''

Défi Turing 07
==============

Quel est le 23456ème nombre premier ?

En énumérant les six premiers nombres premiers : 2, 3, 5, 7, 11 et 13, on voit
que le 6ème nombre premier est 13.

Quel est le 23456ème nombre premier ?

'''

# def sieve(n):

#     primes = [2]

#     # [0, 0, ..., 0] ==> n élément. sieve[i] == 1 <==> n'est pas premier à la fin de l'algorithme
#     sieve = [0] * n
#     for i in [0,1,2]:
#         sieve[i] = 1

#     pivot = 2

#     for i in range(pivot, n, )


'''

On va commencer par une méthode de gros bourrin qui est possible à un débutant
en début d'OCI ... Cela consiste à tester tous les nombres de 1 à ??? jusqu'à
ce qu'on ait accumulé les N premiers nombres premiers, sans crible. 

Ensuite, on pourra optimiser en utilisant un crible (Par exemple Eratosthène
ou mieux encore, Atkin) en connaissant une borne supérieure pour le nombre
cherché.

Question : a-t-on une idée de la taille du n-ième nombre premier
(borne supérieure par exemple) ?

Réponse : selon le théorème de G. Robin (La preuve nécessite des notions
avancées de théorie de nombres)
(https://www.maa.org/sites/default/files/jaroma03200545640.pdf),  n est
inférieur à

    >>> n * log(n) + n * (log(log(n)) - 0.9385)
    268177.9641512513

Ceci permet de mettre facilement en place un crible d'Eratosthène en allouant
d'entrée de jeu un tableau de 

N = 268178
candidates = array([0 for i in range(N)])

On utilise un array() pour que la mémoire soit moins gaspillée (nul besoin
d'utiliser une liste pour cette opération)

'''

import time

def find_divisor(n, first_primes):

    for d in first_primes:
        if n % d == 0:
            return True

    return False


def first_primes(n):

    count = 1
    n_first_primes = [2]
    current_n = 3

    while count < n:
        if not find_divisor(current_n, n_first_primes):
            n_first_primes += [current_n]
            count += 1
            print(count)

        current_n += 2

    return n_first_primes


def solve():
    '''

    Algorithme naïf utilisant la force brute ... pas très rapide (environ 50 secondes)

    '''

   return first_primes(23456)[-1]


if __name__ == '__main__':
    t1 = time.clock()
    reponse = solve()
    t2 = time.clock()
    print("Réponse =", reponse)
    print("Temps nécessaire pour trouver la réponse : ", (t2-t1), "secondes")


'''

Réponse = 267649
Temps n▒cessaire pour trouver la r▒ponse :  50.85625611176933 secondes

'''