'''

Défi Turing 09
==============

Le triplet d'entiers naturels non nuls (a,b,c) est pythagoricien si a2 + b2 =
c2. Par exemple, (3,4,5) est un triplet pythagoricien.

Parmi les triplets pythagoriciens (a,b,c) tels que a + b + c = 3600, donner le
produit a x b x c le plus grand.

'''

import time
from math import sqrt, floor


class MaxFound(object):

    def __init__(self):

        self.a = 1
        self.b = 1
        self.c = 1
        self.produit = 1

    def update(self, values):
        (self.a, self.b, self.c, self.produit) = values

    def __str__(self):
        return "(a, b, c, produit) : ({}, {}, {}, {})\nsomme : {}".format(
            self.a,
            self.b,
            self.c,
            self.produit,
            self.a + self.b + self.c
        )


def solve():

    '''

    Algorithme qui tente d'énumérer le moins de couples possibles. L'idée est déviter dans tous les cas la triple boucle

    ::

        for a in range(1, N):
            for b in range(1, N):
                for c in range(1, N):
                    test(a, b, c)

    car la grande majorité de ces combinaisons ne vont évidemment pas
    fonctionner. Il suffit déjà d'énumérer les (a,b) et de vérifier que 

    ..  math:: 

        a^2 + b^2 = c^2 

    et d'autre part que 

    ..  math::

        c = 3600 - (a + b)


    De plus, on peut éviter tous les doublons ``(a,b) == (b,a)``. Pour casser
    la symétrie, il suffit d'imposer dans la boucle que :math:`b > a` :

    ::

        for a in range(1, )

    '''

    N = 3600

    result = MaxFound()

    for a in range(N // 3, 1, -1):
        for b in range(3600 - 2 * a + 1, a, -1):
            c = 3600 - (a + b)

            if a**2 + b**2 == c**2:
                produit = a * b * c
                if produit > result.produit:
                    result.update((a, b, c, produit))

                if produit < (N / 3)**3 / 2:
                    # le produit est trop petit pour être optimal
                    return result

    return result


if __name__ == '__main__':
    t1 = time.clock()
    reponse = solve()
    t2 = time.clock()
    print(reponse)
    print("Temps nécessaire pour trouver la réponse : ", (t2-t1), "secondes")
