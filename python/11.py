'''

Défi Turing 11
==============

On appellera "miroir d'un nombre n" le nombre n écrit de droite à gauche. Par
exemple, miroir(7423) = 3247.

Quel est le grand grand nombre inférieur à 10 millions ayant la propriété :
miroir(n) = 4 x n ?

'''

import time

def mirroir(n):
    result = int("".join(reversed(str(n))))
##    print(n, result)
    return result

def solve():

    # Borne maximale
    N = int(1e7)

    # puisque l'on s'intéresse au plus grand candidat possible, on commence
    # par tester les plus grands possibles. On retourne le premier que l'on
    # rencontre
    for k in range(N, 1, -1):
##        print(k)
        if mirroir(k) == 4 * k:
            return k

    # on retourne -1 si l'on n'a rien trouvé
    return -1


if __name__ == '__main__':
    t1 = time.clock()
    reponse = solve()
    t2 = time.clock()
    print(reponse)
    print("Temps nécessaire pour trouver la réponse : ", (t2-t1), "secondes")
