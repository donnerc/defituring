# README #

Ce dépôt contient mes versions des défis Turing rédigés par Didier Müller (www.apprendre-en-ligne.net/turing/index.php). 

## Comment se servir de ce dépôt

Ce dépôt permet de réviser les notions de Python vues au cours d'Option complémentaire informatique. Il faut à tout prix éviter de regarder les solutions avant d'avoir cherché de toutes les manières possibles de résoudre les défis proposés. C'est le processus de recherche qui est intéressant en lui-même et non le programme solution. Programmer, ce n'est pas avant tout connaître toutes les subtilités d'un langage tel que Python, mais être capable de trouver des solutions originales par soi-même. Pour chacun des problèmes, prenez du temps pour chercher les solutions par vous-mêmes avant de regarder les solutions.

Dans l'idéal, il ne faudrait consulter les solutions qu'après avoir réussi à résoudre les défis, dans le but de voir comment améliorer votre pratique du Python.

Les corrigés se trouvent dans le dossier `python` et son numéroté `<no_defi>.py`.

## Ressources disponibles

Section à compléter